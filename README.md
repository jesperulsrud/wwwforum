# README #

dependencies used
"dependencies": {
    "body-parser": "^1.19.0",
    "ejs": "^3.1.6",
    "express": "^4.17.1"
  }
  
I've been using nodemon to run the server.

### What is this repository for? ###

* Forum webapp for the www-technologies course

### App locations ###

* http://localhost:8080/			- start page, log in or create user
* http://localhost:8080/login		- login page
* http://localhost:8080/register	- page for creating new user
* http://localhost:8080/userpage	- userpage with different options for users with different roles - admins can get a list over mod applications here and respond to them
* http://localhost:8080/threads		- the actual forum, with posts sorted by likes, here you can to each post to check out comments, or delete post or comment if you have the right
* http://localhost:8080/userlist	- mods and admins can view a list of users, does not show the blocked ones
* http://localhost:8080/blocked		- mods and admins can view a list of blocked users and deleted posts

### How to use the forum ###

* You either log in or create a user from the first page. None of the other pages should be accessible without logging in, you will be redirected to the index page. When you create a new user you will be automatically loged in.
* After you have logged in, you get to the user page. From there you have different options depenging on what role you have, admin, moderator or user. As a user you can go to /threads and create a new post, or see other post and like and comment. As a Mod you can block users and delete posts, see a list of all users, and all the things a user can. As an admin you can accept users applications for becoming moderators, and see a list of all users, mods and admins, and all the things a mod can do.
* From the userpage you can go to threads. They are all sorted on amount of likes. You can go to each post, where you can like, or comment, or delete psot or comment if you are a moderator or admin.
* From the userpage you can go to userlist if you are admin or moderator, moderators will see a list of all users, admins will se a list of all admins, moderators and users.
* From the userpage you can go to blocked if you are a moderator or admin, there you can see a list of all blocked users and all deleted posts.

### Notes on usage ###

* To create a user you need to have an email, with an "@", and the password has to be at least 6 characters long.
* Admins has to be manually created from within the firebase console.
* After a new comment has been made on a post, the delete function on the comments does not work, so has to reload to get it to work.
* After pressing the delete button on a comment, the comment is deleted from firestore, but the list the user will see will not be updated before page is reloaded. 
* To reload the comments, you have to go back to /threads and go back into the same post.
* After liking a post, like counter will not be updated until /threads is reloaded. As it is now, a user can like a post multiple times, if reloading the page.
* There are provided som users for testing. Some posts are added to the threads, and some comments added to some of the posts. There are one user currently blocked, and one post deleted, which you can see in /blocked if you log on as an admin og moderator.
* There should be a couple of applies for moderators, you can review them from the /userpage if you are logged in as an admin.

### List of users currently created ###

* same password for all users - 123456
* user1@test.no
* user2@test.no
* user3@test.no
* user4@test.no
* mod@test.no
* admin@test.no






