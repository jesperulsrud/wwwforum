var express = require("express");
var app = express();

app.set("view engine", "ejs");
app.use("/assets", express.static("assets"));

app.get("/", function (req, res) {
  res.render("index");
});

app.get("/login", function (req, res) {
  res.render("login");
});

app.get("/register", function (req, res) {
  res.render("register");
});

app.get("/userpage", function (req, res) {
  res.render("userpage");
});

app.get("/threads", function (req, res) {
  res.render("threads");
});

app.get("/userlist", function (req, res) {
  res.render("userlist");
});

app.get("/blocked", function (req, res) {
  res.render("blocked");
});

app.listen(8080);
