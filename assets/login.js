// login
const loginForm = document.querySelector("#login-form");
loginForm.addEventListener("submit", (e) => {
  e.preventDefault();

  // get user info
  const email = loginForm["login-email"].value;
  const password = loginForm["login-password"].value;

  console.log(email, password);

  // log in the user
  auth.signInWithEmailAndPassword(email, password).then((cred) => {
    loginForm.reset();
    window.location = "/userpage";
  });
});
