// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
var firebaseConfig = {
  apiKey: "AIzaSyBCO7MhCjua5QMR1R7RVnuDoIiLDWP0uQc",
  authDomain: "web-forum-6dd43.firebaseapp.com",
  projectId: "web-forum-6dd43",
  storageBucket: "web-forum-6dd43.appspot.com",
  messagingSenderId: "963420518100",
  appId: "1:963420518100:web:a43a9fa591bbd577762d41",
  measurementId: "G-KC3LCD300B",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// make auth and firestore references
const auth = firebase.auth();
const db = firebase.firestore();

// update firestore settings
db.settings({ timestampsInSnapshots: true });
