// signup
const signupForm = document.querySelector("#signup-form");
signupForm.addEventListener("submit", (e) => {
  e.preventDefault();

  // get user info
  const email = signupForm["signup-email"].value;
  const password = signupForm["signup-password"].value;
  const posts = [];

  // sign up the user and add user to firestore collection
  auth
    .createUserWithEmailAndPassword(email, password)
    .then((cred) => {
      return db.collection("users").doc(cred.user.uid).set({
        username: signupForm["signup-username"].value,
        email: email,
        posts: posts,
        role: "user",
      });
    })
    .then(() => {
      signupForm.reset();
      window.location = "/userpage";
    });
});
