const postDiv = document.querySelector(".post-div");
const commentList = document.querySelector(".comments");
const postsList = document.getElementById("post-table");

function getPost(postid, loggedInUser, role) {
  postsList.style.display = "none";

  // get the specified post
  db.collection("threads")
    .doc(postid)
    .get()
    .then((doc) => {
      const post = doc.data().post;
      const username = doc.data().username;
      const likes = doc.data().likes;
      const commentArray = doc.data().comments;

      // post html file DOES NOT WORK FOR SOME REASON, overwriting threads.ejs instead
      //window.location = "/post";
      setupPost(
        post,
        username,
        likes,
        commentArray,
        loggedInUser,
        postid,
        role
      );
    });
}

// show post and all comments
const setupPost = (
  post,
  username,
  likes,
  commentArray,
  loggedInUser,
  postid,
  role
) => {
  var div = ``;
  // show post - with delete option if admin or moderator
  if (role == "user") {
    div = `
    <p>by: ${username} - ${likes} likes</p>
    <textarea readonly style="resize: none;" id="post" rows="5" cols="60"> ${post} </textarea>
    <button id="like${postid}">Like</button>
  `;
  } else {
    div = `
    <p>by: ${username} - ${likes} likes</p>
    <textarea readonly style="resize: none;" id="post" rows="5" cols="60"> ${post} </textarea>
    <button id="like${postid}">Like</button>
    <button id="delete${postid}">Delete</button>
  `;
  }

  postDiv.innerHTML = div;

  // eventlistener for like and delete buttons on the post
  document
    .getElementById("like" + postid)
    .addEventListener("click", function () {
      likePost(likes, postid);
    });

  if (role != "user") {
    document
      .getElementById("delete" + postid)
      .addEventListener("click", function () {
        deletePost(postid, post, username);
      });
  }

  // display the comments
  showComments(commentArray, role, postid);

  // create new comment
  const commentForm = document.querySelector("#comment-form");
  commentForm.style.display = "block";
  commentForm.addEventListener("submit", (e) => {
    e.preventDefault();

    newComment = commentForm["comment"].value;
    commentArray.push(newComment);

    // add new comment to post in firestore
    db.collection("threads")
      .doc(postid)
      .update({
        comments: commentArray,
      })
      .then(() => {
        commentForm.reset();
        showComments(commentArray);
      });
  });

  /*
  // show comments
  let html = "";
  if (commentArray) {
    commentArray.forEach((doc) => {
      const li = `
          <li>
              <div>
                <textarea readonly style="resize: none;" rows="4" cols="40"> ${doc} </textarea>
              </div>
          </li>
      `;
      html += li;
      commentList.innerHTML = html;
    });
  } else commentList.innerHTML = html;
  */
};

function likePost(likes, postid) {
  // update like field in post database collection
  likes++;

  var postRef = db.collection("threads").doc(postid);

  // Set the 'capital' field of the city
  var updateSingle = postRef.update({ likes: likes });

  const btn = document.getElementById("like" + postid);
  btn.disabled = true;
}

function deletePost(postid, post, username) {
  // deleting post from threads in firestore
  db.collection("threads")
    .doc(postid)
    .delete()
    .then(() => {
      console.log(postid + " was deleted");
      window.location = "/threads";

      // adding deleted post to blockedPosts in firestore
      db.collection("blockedPosts").doc(postid).set({
        post: post,
        username: username,
      });
    });
}

function showComments(commentArray, role, postid) {
  // show comments - with delete button if admin or mod

  let html = "";
  if (commentArray) {
    if (role == "user") {
      commentArray.forEach((doc) => {
        const li = `
            <li>
                <div>
                  <textarea readonly style="resize: none;" rows="4" cols="40"> ${doc} </textarea>
                </div>
            </li>
        `;
        html += li;
        commentList.innerHTML = html;
      });
    } else {
      commentArray.forEach((doc) => {
        const li = `
            <li>
                <div>
                  <textarea readonly style="resize: none;" rows="4" cols="40"> ${doc} </textarea>
                  <button id="cmtdelete${doc}">Delete</button>
                </div>
            </li>
        `;
        html += li;
        commentList.innerHTML = html;
      });

      // event listener for delete button for comment
      commentArray.forEach((doc) => {
        if (role != "user") {
          document
            .getElementById("cmtdelete" + doc)
            .addEventListener("click", function () {
              deleteComment(postid, doc);
            });
        }
      });
    }
  } else commentList.innerHTML = html;
}

// deletes comment - DOES NOT WORK AFTER NEW COMMENT HAS BEEN CREATED, NEED TO REFRESH FIRST
function deleteComment(postid, doc) {
  db.collection("threads")
    .doc(postid)
    .update({
      comments: firebase.firestore.FieldValue.arrayRemove(doc),
    })
    .then(() => {
      console.log("comment was deleted");
      //window.location = "/threads";
    });
}
