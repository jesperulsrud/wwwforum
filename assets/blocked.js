// listen for status change
auth.onAuthStateChanged((user) => {
  if (!user) {
    window.location = "/";
    console.log("restricted access");
  } else {
    const blockedUserList = [];
    const blockedPostList = [];

    // getting all blocked users and adding into the list
    db.collection("blockedUsers").onSnapshot((snapshot) => {
      snapshot.docs.forEach((doc) => {
        const user = doc.data();
        blockedUserList.push({ username: user.username, userid: doc.id });
      });

      const userList = document.querySelector(".blocked-users");

      // displaying users to the UI
      let userhtml = ``;
      blockedUserList.forEach((doc) => {
        const li = `
          <li>
            <p> ${doc.username} </p>
          </li>
      `;
        userhtml += li;
      });

      userList.innerHTML = userhtml;
    });

    // getting all blocked/deleted posts and adding into the list
    db.collection("blockedPosts").onSnapshot((snapshot) => {
      snapshot.docs.forEach((doc) => {
        const post = doc.data();
        blockedPostList.push({ post: post.post, username: post.username });
      });

      const postList = document.querySelector(".blocked-posts");

      // displaying posts to the UI
      let posthtml = ``;
      blockedPostList.forEach((doc) => {
        const li = `
        <div>
        <p>by: ${doc.username}</p>
        <textarea readonly style="resize: none;" id="post" rows="5" cols="60"> ${doc.post} </textarea>
    </div>
      `;
        posthtml += li;
      });

      postList.innerHTML = posthtml;
    });
  }
});
