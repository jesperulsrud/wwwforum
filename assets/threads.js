// listen for status changes
auth.onAuthStateChanged((user) => {
  if (!user) {
    window.location = "/";
    console.log("please log in");
  } else {
    // checking if a user in blocked
    db.collection("users")
      .doc(user.uid)
      .get()
      .then((doc) => {
        if (doc.data().role == "blocked") {
          window.location = "/userpage";
          console.log("you have been blocked from posting");
        }
      });

    // getting username
    db.collection("users")
      .doc(user.uid)
      .get()
      .then((doc) => {
        role = doc.data().role;
        const postUsername = doc.data().username;
        const postLikes = 0;

        // getting all the posts from firestore
        db.collection("threads").onSnapshot((snapshot) => {
          setupPosts(snapshot.docs, postUsername, role);
        });

        // create new post from the form
        const postForm = document.querySelector("#post-form");
        postForm.addEventListener("submit", (e) => {
          e.preventDefault();

          // adding to firestore
          db.collection("threads")
            .add({
              likes: postLikes,
              post: postForm["post"].value,
              username: postUsername,
              comments: [],
            })
            .then(() => {
              postForm.reset();
            });
        });
      });
  }
});

// setup posts
const postList = document.querySelector(".posts");

const setupPosts = (data, username, role) => {
  let html = "";

  var threadArray = [];
  var sorted = [];

  // sort posts by likes
  data.forEach((doc) => {
    const dbpost = doc.data().post;
    const dblikes = doc.data().likes;
    const dbuser = doc.data().username;
    const pid = doc.id;

    obj = {
      post: dbpost,
      likes: dblikes,
      username: dbuser,
      postid: pid,
    };

    sorted.push(obj);
  });

  // sorting array by likes
  sorted.sort(function (a, b) {
    return a.likes - b.likes;
  });

  threadArray = sorted.reverse();

  // displaying all posts
  threadArray.forEach((doc) => {
    const li = `
        <li>
            <div>
                <p>by: ${doc.username} - ${doc.likes} likes</p>
                <textarea readonly style="resize: none;" id="post" rows="5" cols="60"> ${doc.post} </textarea>
                <button id="btn${doc.postid}">Go to post</button>
            </div>
        </li>
    `;
    html += li;
  });
  postList.innerHTML = html;

  // setting the button for each post
  threadArray.forEach((doc) => {
    document
      .getElementById("btn" + doc.postid)
      .addEventListener("click", function () {
        getPost(doc.postid, username, role);
      });
  });

  /* 
  // display posts without sorting
  data.forEach((doc) => {
    const post = doc.data();
    const postId = doc.id;
    const li = `
        <li>
            <div>
                <p>by: ${post.username} - ${post.likes} likes</p>
                <textarea readonly style="resize: none;" id="post" rows="5" cols="60"> ${post.post} </textarea>
                <button id="btn${postId}">Go to post</button>
            </div>
        </li>
    `;
    html += li;
  });
  postList.innerHTML = html;

  data.forEach((doc) => {
    const post = doc.data();
    const postId = doc.id;

    document
      .getElementById("btn" + postId)
      .addEventListener("click", function () {
        getPost(postId, username, role);
      });
  });
  */
};
