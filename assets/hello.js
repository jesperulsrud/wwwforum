var http = require("http");
var fs = require('fs');

http.createServer(function(request, response) {
    response.writeHead(200, {'Context-Type': 'text/html'});
    var myReadStream = fs.createReadStream(__dirname + '/login.html', 'utf8');
    myReadStream.pipe(response);
}).listen(8080);
console.log('listening on port 8080...');