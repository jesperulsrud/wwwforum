// display account details
const accountDetails = document.querySelector(".account-details");
var applyButton = document.getElementById("applyButton");
var reviewButton = document.getElementById("reviewButton");
var listButton = document.getElementById("listButton");
var blockedButton = document.getElementById("blockedButton");

// listen for status change
auth.onAuthStateChanged((user) => {
  var usernameApplied = "";
  if (!user) {
    window.location = "/";
    console.log("please log in");
    accountDetails.innerHTML = "";
  } else {
    var role;
    // checking user in firestore for their role
    db.collection("users")
      .doc(user.uid)
      .get()
      .then((doc) => {
        role = doc.data().role;
        usernameApplied = doc.data().username;
        if (role == "admin") {
          // if admin show the review button
          reviewButton.style.display = "block";
          listButton.style.display = "block";
          blockedButton.style.display = "block";
        } else if (role == "user") {
          applyButton.style.display = "block";
          // if regular user show apply for mod button
        } else if (role == "moderator") {
          // if moderator show userlist button and blocked button
          listButton.style.display = "block";
          blockedButton.style.display = "block";
        }
        const html = `
              <div> Logged in as: ${
                doc.data().username
              } - Status: ${role} </div>
            `;
        accountDetails.innerHTML = html;
      });

    // eventlistener for the apply for moderator button
    document
      .getElementById("applyButton")
      .addEventListener("click", function () {
        db.collection("applies")
          .doc(user.uid)
          .set({
            username: usernameApplied,
            userid: user.uid,
          })
          .then(() => {
            console.log("Successfully applied for moderator");
          });
      });

    // eventlistener for the review mod applies button
    document
      .getElementById("reviewButton")
      .addEventListener("click", function () {
        reviewApplies();
      });

    // eventlistener for the apply for user list button
    document
      .getElementById("listButton")
      .addEventListener("click", function () {
        window.location = "/userlist";
      });

    // eventlistener for the apply for blocked users and posts button
    document
      .getElementById("blockedButton")
      .addEventListener("click", function () {
        window.location = "/blocked";
      });
  }
});

const applyList = document.querySelector(".applies");
const applies = document.getElementById("applies-table");

// show list of applications
function reviewApplies() {
  applies.style.display = "block";
  db.collection("applies").onSnapshot((snapshot) => {
    let html = "";
    // loop all applies and set the html string for the ui
    snapshot.docs.forEach((doc) => {
      const apply = doc.data();
      const li = `
        <li>
            <div>
                <p>user: ${apply.username} </p>
                <button id="btnmod${apply.userid}">Make moderator</button>
                <button id="btndecline${apply.userid}">Decline</button><br/><br/>
            </div>
        </li>
    `;
      html += li;
    });
    applyList.innerHTML = html;

    snapshot.docs.forEach((doc) => {
      const apply = doc.data();

      // eventlistener for accept mod apply button
      document
        .getElementById("btnmod" + apply.userid)
        .addEventListener("click", function () {
          makeMod(apply.userid);
        });

      // eventlistener for decline mod apply button
      document
        .getElementById("btndecline" + apply.userid)
        .addEventListener("click", function () {
          declineMod(apply.userid);
        });
    });
  });
}

// make a user moderator
function makeMod(userid) {
  console.log(userid);

  // get user and update role to mod
  db.collection("users")
    .doc(userid)
    .update({
      role: "moderator",
    })
    .then(() => {
      // delete application document
      db.collection("applies")
        .doc(userid)
        .delete()
        .then(() => {
          // refresh application list
          reviewApplies();
        });
    });
}

// decline users application to become mod
function declineMod(userid) {
  // delete apply entry from firestore
  db.collection("applies")
    .doc(userid)
    .delete()
    .then(() => {
      reviewApplies();
    });
}

// logout
function logout() {
  auth.signOut().then(() => {
    console.log("user signed out");
    window.location = "/";
  });
}

function goToThreads() {
  // to to threads page

  window.location = "/threads";
}
