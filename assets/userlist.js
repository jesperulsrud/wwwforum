// listen for status changes
auth.onAuthStateChanged((user) => {
  if (!user) {
    window.location = "/";
    console.log("please log in");
  } else {
    const adminList = [];
    const modList = [];
    const userList = [];
    // loop through all users and add them into lists based on their role
    db.collection("users").onSnapshot((snapshot) => {
      snapshot.docs.forEach((doc) => {
        const user = doc.data();
        switch (user.role) {
          case "admin":
            adminList.push({ username: user.username, userid: doc.id });
            break;
          case "moderator":
            modList.push({ username: user.username, userid: doc.id });
            break;
          case "user":
            userList.push({ username: user.username, userid: doc.id });
            break;
        }
      });

      const adminUl = document.querySelector(".admins");
      const modUl = document.querySelector(".moderators");
      const userUl = document.querySelector(".users");

      // setting the UI to display each user from each list
      let adminhtml = ``;
      adminList.forEach((doc) => {
        const li = `
            <li>
              <p>admin: ${doc.username} </p>
            </li>
        `;
        adminhtml += li;
      });

      let modhtml = ``;
      modList.forEach((doc) => {
        const li = `
              <li>
                <p>moderator: ${doc.username} </p>
              </li>
          `;
        modhtml += li;
      });

      let userhtml = ``;
      userList.forEach((doc) => {
        const li = `
              <li>
                <p>user: ${doc.username} </p>
                <button id="block${doc.userid}">Block</button>
              </li>
          `;
        userhtml += li;
      });

      adminUl.innerHTML = adminhtml;
      modUl.innerHTML = modhtml;
      userUl.innerHTML = userhtml;

      // checking role and displaying appropriate data
      db.collection("users")
        .doc(user.uid)
        .get()
        .then((doc) => {
          role = doc.data().role;
          switch (doc.data().role) {
            case "admin":
              adminUl.style.display = "block";
              modUl.style.display = "block";
              userUl.style.display = "block";
              break;
            case "moderator":
              userUl.style.display = "block";
              break;
            case "user":
              window.location = "/userpage";
              console.log("restricted access");
              break;
            case "blocked":
              window.location = "/userpage";
              console.log("restricted access");
              break;
          }
        });

      // eventlistener for block user button
      userList.forEach((doc) => {
        document
          .getElementById("block" + doc.userid)
          .addEventListener("click", function () {
            blockUser(doc.userid, doc.username);
          });
      });
    });
  }
});

function blockUser(uid, user) {
  // change role from user to blocked
  db.collection("users")
    .doc(uid)
    .update({
      role: "blocked",
    })
    .then(() => {
      // add user to blockedUsers in firestore
      db.collection("blockedUsers")
        .doc(uid)
        .set({
          username: user,
          userid: uid,
        })
        .then(() => {
          // refresh userlist
          window.location = "/userlist";
        });
    });
}
